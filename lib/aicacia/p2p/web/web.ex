defmodule Aicacia.P2P.Web do
  def controller do
    quote do
      use Phoenix.Controller, namespace: Aicacia.P2P.Web

      import Plug.Conn
      alias Aicacia.P2P.Web.Router.Helpers, as: Routes
    end
  end

  def plugs do
    quote do
      use Phoenix.Controller, namespace: Aicacia.P2P.Web
      import Plug.Conn
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/aicacia/p2p/web/templates",
        namespace: Aicacia.P2P.Web

      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      alias Aicacia.P2P.Web.Router.Helpers, as: Routes
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
    end
  end

  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
