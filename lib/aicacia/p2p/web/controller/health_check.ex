defmodule Aicacia.P2P.Web.Controller.HealthCheck do
  use Aicacia.P2P.Web, :controller

  action_fallback Aicacia.P2P.Web.Controller.Fallback

  def health(conn, _params) do
    conn
    |> json(%{ ok: true })
  end
end
