defmodule Aicacia.P2P.Web.Controller.Fallback do
  use Aicacia.P2P.Web, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(Aicacia.P2P.Web.View.Error)
    |> render(:"404")
  end
end
