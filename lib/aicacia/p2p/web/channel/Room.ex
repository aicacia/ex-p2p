defmodule Aicacia.P2P.Web.Channel.Room do
  use Aicacia.P2P.Web, :channel

  def join("room:" <> room_id, %{"data" => data}, socket) do
    socket =
      socket
      |> assign(:data, data)
      |> assign(:room_id, room_id)

    send(self(), :after_join)
    {:ok, socket}
  end

  def terminate(_reason, socket) do
    broadcast_from!(socket, "peer_leave", %{peer_id: socket.assigns.uuid})
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    broadcast_from!(socket, "peer_join", %{
      peer_id: socket.assigns.uuid,
      data: socket.assigns.data
    })

    {:noreply, socket}
  end

  def handle_info(
        %{event: "phx_leave"},
        socket
      ) do
    broadcast_from!(socket, "peer_leave", %{peer_id: socket.assigns.uuid})
    {:stop, {:shutdown, :left}, socket}
  end

  def handle_info({:DOWN, _, _, reason}, %{} = socket) do
    reason = if reason == :normal, do: {:shutdown, :closed}, else: reason
    broadcast_from!(socket, "peer_leave", %{peer_id: socket.assigns.uuid})
    {:stop, reason, socket}
  end

  def handle_in("phx_leave", socket) do
    broadcast_from!(socket, "peer_leave", %{peer_id: socket.assigns.uuid})
    {:noreply, socket}
  end

  def handle_in("discover_peers", _payload, socket) do
    broadcast_from!(socket, "request_peer", %{requestor_id: socket.assigns.uuid})
    {:noreply, socket}
  end

  def handle_in(
        "send_peer_to",
        %{"requestor_id" => requestor_id, "peer_id" => peer_id, "data" => data},
        socket
      ) do
    broadcast_from!(socket, "discovered_peer", %{
      requestor_id: requestor_id,
      peer_id: peer_id,
      data: data
    })

    {:noreply, socket}
  end

  def handle_in(
        "send_discovered_peer_to",
        %{"requestor_id" => requestor_id, "peer_id" => peer_id, "data" => data},
        socket
      ) do
    if requestor_id == socket.assigns.uuid do
      push(socket, "peer_join", %{peer_id: peer_id, data: data})
    end

    {:noreply, socket}
  end

  def handle_in("update_peer_data", data, socket) do
    socket = assign(socket, :data, data)
    broadcast_from!(socket, "peer_data_updated", %{peer_id: socket.assigns.uuid, data: data})
    {:noreply, socket}
  end

  def handle_in("broadcast", payload, socket) do
    broadcast_from!(socket, "message", %{from_id: socket.assigns.uuid, payload: payload})
    {:noreply, socket}
  end

  def handle_in(
        "send_to",
        %{"from_id" => from_id, "to_id" => to_id, "payload" => payload},
        socket
      ) do
    if to_id == socket.assigns.uuid do
      broadcast_from!(socket, "message", %{from_id: from_id, payload: payload})
    end

    {:noreply, socket}
  end
end
