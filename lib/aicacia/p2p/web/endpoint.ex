defmodule Aicacia.P2P.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :aicacia_p2p

  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  socket "/socket", Aicacia.P2P.Web.Socket.User,
    websocket: true,
    longpoll: false

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  plug CORSPlug

  plug Aicacia.P2P.Web.Router
end
