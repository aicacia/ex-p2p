defmodule Aicacia.P2P.Web.Socket.User do
  use Phoenix.Socket

  channel "room:*", Aicacia.P2P.Web.Channel.Room

  def connect(params, socket, _connect_info) do
    {:ok,
     socket
     |> assign(:uuid, Map.get(params, "id", UUID.uuid4()))
     |> assign(:data, Map.get(params, "data", %{}))}
  end

  def id(socket), do: "user:#{socket.assigns.uuid}"
end
