defmodule Aicacia.P2P.Web.Router do
  use Aicacia.P2P.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Aicacia.P2P.Web.Controller do
    pipe_through :api

    get "/health", HealthCheck, :health
    head "/health", HealthCheck, :health
  end
end
