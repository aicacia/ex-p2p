defmodule Aicacia.P2P.Application do
  use Application
  import Supervisor.Spec, warn: false

  def start(_type, _args) do
    children = [
      {Phoenix.PubSub, [name: Aicacia.P2P.Web.PubSub, adapter: Phoenix.PubSub.PG2]},
      Aicacia.P2P.Web.Endpoint
    ]

    opts = [strategy: :one_for_one, name: Aicacia.P2P.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Aicacia.P2P.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
