defmodule Aicacia.P2P.MixProject do
  use Mix.Project

  def organization do
    :aicacia
  end

  def name do
    :p2p
  end

  def version do
    "0.1.0"
  end

  def project do
    [
      app: String.to_atom("#{organization()}_#{name()}"),
      version: version(),
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Aicacia.P2P.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp env(:prod),
    do: [
      DOCKER_REGISTRY: "registry.#{organization()}.com",
      HELM_REPO: "https://chartmuseum.#{organization()}.com"
    ]

  defp env(_),
    do: [
      DOCKER_REGISTRY: "registry.local-k8s.com",
      HELM_REPO: "http://chartmuseum.local-k8s.com"
    ]

  defp deps do
    [
      {:phoenix, "~> 1.5"},
      {:phoenix_pubsub, "~> 2.0"},
      {:postgrex, ">= 0.0.0"},
      {:jason, "~> 1.2"},
      {:uuid, "~> 1.1"},
      {:cors_plug, "~> 2.0"},
      {:plug_cowboy, "~> 2.3"},
      {:peerage, "~> 1.0"},
      {:distillery, "~> 2.1"}
    ]
  end

  defp namespace(), do: "api"
  defp helm_dir(), do: "./helm/#{organization()}-#{name()}"
  defp get_env(key), do: Keyword.get(env(Mix.env()), key, "")

  # defp docker_repository(), do: "#{get_env(:DOCKER_REGISTRY)}/api/#{name()}"
  defp docker_repository(), do: "registry.gitlab.com/aicacia/ex-p2p"
  defp docker_tag(), do: "#{docker_repository()}:#{version()}"

  defp helm_overrides(),
    do:
      "--set image.tag=#{version()} --set image.repository=#{docker_repository()} --set image.hash=$(mix docker.sha256)"

  defp createHelmInstall(values \\ nil),
    do:
      "helm install #{helm_dir()} --name #{name()} --namespace=#{namespace()} #{helm_overrides()} #{
        if values == nil, do: "", else: "--values #{values}"
      }"

  defp createHelmUpgrade(values \\ nil),
    do:
      "helm upgrade #{name()} #{helm_dir()} --namespace=#{namespace()} --install #{
        helm_overrides()
      } #{if values == nil, do: "", else: "--values #{values}"}"

  defp aliases do
    [
      # Docker
      "docker.build": ["cmd docker build --build-arg MIX_ENV=#{Mix.env()} -t #{docker_tag()} ."],
      "docker.push": ["cmd docker push #{docker_tag()}"],
      "docker.run": ["cmd docker run -d -e MIX_ENV=#{Mix.env()} -p 4000:4000 #{docker_tag()}"],
      "docker.sha256": [
        ~s(cmd docker inspect --format='"{{index .Id}}"' #{docker_tag()})
      ],

      # Helm
      "helm.push": [
        "cmd cd #{helm_dir()} && helm push . #{get_env(:HELM_REPO)} --username=\"#{
          get_env(:HELM_REPO_USERNAME)
        }\" --password=\"#{get_env(:HELM_REPO_PASSWORD)}\""
      ],
      "helm.delete": ["cmd helm delete --namespace #{namespace()} #{name()}"],
      "helm.install": ["cmd #{createHelmInstall()}"],
      "helm.install.local": ["cmd #{createHelmInstall("#{helm_dir()}/values-local.yaml")}"],
      "helm.upgrade": ["cmd #{createHelmUpgrade()}"],
      "helm.upgrade.local": ["cmd #{createHelmUpgrade("#{helm_dir()}/values-local.yaml")}"],
      helm: [
        "docker.build",
        "docker.push",
        "helm.upgrade"
      ],
      "helm.local": [
        "docker.build",
        "docker.push",
        "helm.upgrade.local"
      ]
    ]
  end
end
