use Mix.Config

config :aicacia_p2p, Aicacia.P2P.Web.Endpoint,
  http: [:inet6, port: 4000],
  url: [host: "localhost", port: 4000]

config :logger, level: :info

config :peerage,
  via: Peerage.Via.Dns,
  dns_name: "p2p-aicacia-p2p.api"
