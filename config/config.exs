use Mix.Config

config :aicacia_p2p,
  generators: [binary_id: true]

config :aicacia_p2p, Aicacia.P2P.Web.Endpoint,
  url: [host: "localhost"],
  check_origin: false,
  secret_key_base: "wF2JEGZt8htPRCCWxuDaB05lZDEhg8pIH72uX3LUsEW9JhnyBtOljer7JN4wyEdY",
  render_errors: [view: Aicacia.P2P.Web.View.Error, accepts: ~w(json)],
  pubsub_server: Aicacia.P2P.Web.PubSub

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

config :aicacia_p2p, Aicacia.P2P.Scheduler, debug_logging: true

config :peerage,
  via: Peerage.Via.Dns,
  dns_name: "localhost",
  app_name: "aicacia_p2p"

import_config "#{Mix.env()}.exs"
