use Mix.Config

config :aicacia_p2p, Aicacia.P2P.Web.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
